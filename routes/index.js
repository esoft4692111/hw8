const express=require('express');
const router=express.Router();
const accountsDB=require('../db/accountsDB');
const ratingDB=require("../db/ratingDB");
const checkPassword=require("../middleware/checkPassword.js");
const bcrypt=require("bcrypt");

router.get("/auth",(req,res)=>{
    res.render("auth",{
        accounts:accountsDB,
        bcrypt:bcrypt
    })
})


router.get("/rating",(req,res)=>{
    res.render("rating",{
        rating:ratingDB
    });
})

module.exports=router;