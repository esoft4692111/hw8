const bcrypt=require("bcrypt");
const saltRounds = 10;

const checkHashPassword=(hash,password)=>{
    return  bcrypt.compare(hash,password);       
}

module.exports=checkHashPassword;