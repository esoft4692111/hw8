require("dotenv").config();
const express=require("express");
const bcrypt=require("bcrypt");
var path = require("path");
const PORT=process.env.PORT || 5000;
const app=express();

app.set("view engine","ejs");
app.set('views', path.join(__dirname, 'view'));

app.use("/",require("./routes/index.js"));

const start=async ()=>{
    try{
        app.listen(PORT,()=>console.log(`Сервер стартовал на сервере ${PORT}`));
    }catch(e){
        console.log(e);
    }
}

start();